include Rules.mk

ROOTFS_PATH ?= ""
BOOT_PATH ?= ""

all: get-source copy-configs patch-files copy-firmware linux uboot

get-source:
	mkdir -p $(SOURCE_ROOT)
	if [ ! -d "$(LINUX_ROOT)" ]; then \
		cd $(SOURCE_ROOT); \
		wget -c "https://www.kernel.org/pub/linux/kernel/v3.x/$(LINUX_TYPE)/linux-$(LINUX_VERSION).tar.xz"; \
		tar xf linux-$(LINUX_VERSION).tar.xz; \
		rm linux-$(LINUX_VERSION).tar.xz; \
	fi
	if [ ! -d "$(U-BOOT_ROOT)" ]; then \
		cd $(SOURCE_ROOT); \
		git clone git://git.denx.de/u-boot.git; \
		cd "$(U-BOOT_ROOT)" && git checkout $(U-BOOT_VERSION) -b tmp; \
	fi
	if [ ! -d "$(LINARO_TOOLCHAIN)" ]; then \
		cd $(SOURCE_ROOT); \
		wget -c "https://releases.linaro.org/14.03/components/toolchain/binaries/$(LINARO_TOOLCHAIN).tar.xz"; \
		tar xf $(LINARO_TOOLCHAIN).tar.xz; \
		rm $(LINARO_TOOLCHAIN).tar.xz; \
	fi

copy-configs:
	cd configs && $(MAKE)

patch-files:
	cd patches && $(MAKE)	

copy-firmware:
	cd firmware && $(MAKE)

linux:
	cd "$(LINUX_ROOT)" && $(MAKE) ARCH=arm CROSS_COMPILE=$(ARMCC) mrproper
	cd "$(LINUX_ROOT)" && $(MAKE) ARCH=arm CROSS_COMPILE=$(ARMCC) $(LINUX_CONFIG)
	cd "$(LINUX_ROOT)" && $(MAKE) ARCH=arm CROSS_COMPILE=$(ARMCC) $(KERNEL_IMAGE)
	cd "$(LINUX_ROOT)" && $(MAKE) ARCH=arm CROSS_COMPILE=$(ARMCC) modules
	cd "$(LINUX_ROOT)" && $(MAKE) ARCH=arm CROSS_COMPILE=$(ARMCC) dtbs

uboot:
	cd "$(U-BOOT_ROOT)" && $(MAKE) ARCH=arm CROSS_COMPILE=$(ARMCC) distclean
	cd "$(U-BOOT_ROOT)" && $(MAKE) ARCH=arm CROSS_COMPILE=$(ARMCC) $(U-BOOT_CONFIG)
	cd "$(U-BOOT_ROOT)" && $(MAKE) ARCH=arm CROSS_COMPILE=$(ARMCC)

install:
	if ! [ -z $(ROOTFS) ] && ! [ -z $(BOOT) ]; then \
		mkdir $(BOOT)/dtbs/.; \
		cd $(LINUX_ROOT) && $(MAKE) ARCH=arm CROSS_COMPILE=$(ARMCC) INSTALL_MOD_PATH=$(ROOTFS) modules_install; \
		cp $(LINUX_ROOT)/arch/arm/boot/$(KERNEL_IMAGE) $(BOOT)/.; \
		cp $(LINUX_ROOT)/arch/arm/boot/dts/$(DTBS_CONFIG) $(BOOT)/dtbs/.; \
		cp $(U-BOOT_ROOT)/MLO $(BOOT)/.; \
		cp $(U-BOOT_ROOT)/u-boot.img $(BOOT)/.; \
		echo "##################################################################"; \
		echo "# The Default uEnv.txt file is located in the default/ directory #"; \
		echo "##################################################################"; \
	else \
		echo "install requires ROOTFS (directory of root partition)"; \
		echo "install requires BOOT (directory of boot partition)"; \
		echo "make install ROOTFS=/path/to/rootfs BOOT=/path/to/boot"; \
	fi

clean:
	cd configs && $(MAKE) clean
	cd patches && $(MAKE) clean
	cd "$(LINUX_ROOT)" && $(MAKE) ARCH=arm CROSS_COMPILE=$(ARMCC) mrproper
	cd "$(U-BOOT_ROOT)" && $(MAKE) ARCH=arm CROSS_COMPILE=$(ARMCC) distclean
