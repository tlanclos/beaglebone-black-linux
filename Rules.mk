LINUX_TYPE=testing
LINUX_VERSION=3.15-rc2
U-BOOT_VERSION=v2014.04
LINARO_VERSION=4.8-2014.03

IMAGE_TYPE=zImage

BOOT_LOGO=redx

#####################################
# in $(LINUX_ROOT)/arch/arm/configs #
#####################################

LINUX_CONFIG=am335x_custom_defconfig


################################
# in $(U-BOOT_ROOT)/boards.cfg #
################################

U-BOOT_CONFIG=am335x_evm_config


######################################
# in $(LINUX_ROOT)/arch/arm/boot/dts #
######################################

DTBS_CONFIG=am335x-boneblack.dtb

######################################
# DON'T TOUCH THESE VARIABLES PLEASE #
######################################

if [ $(LINUX_TYPE) != "testing" ]; then \
	LINUX_TYPE=""; \
fi

GIT_ROOT=$(shell git rev-parse --show-toplevel)/.
SOURCE_ROOT=$(GIT_ROOT)/source/.
LINUX_ROOT=$(SOURCE_ROOT)/linux-$(LINUX_VERSION)/.
U-BOOT_ROOT=$(SOURCE_ROOT)/u-boot/.
LOGO_ROOT=$(GIT_ROOT)/logos/.

PATCH_DIR=$(GIT_ROOT)/patches/.

LINARO_TOOLCHAIN=$(SOURCE_ROOT)/gcc-linaro-arm-linux-gnueabihf-$(LINARO_VERSION)_linux
ARMCC=$(LINARO_TOOLCHAIN)/bin/arm-linux-gnueabihf-
